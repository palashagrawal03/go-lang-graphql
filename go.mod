module github.com/futuretech123/go-lang-app

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/graphql-go/graphql v0.7.9 // indirect
)
