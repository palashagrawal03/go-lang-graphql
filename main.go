package main

import (
	"crypto/md5"
	"database/sql"
	b64 "encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/graphql-go/graphql"
)

type User struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Twits    []Twit `json:"twits"`
	// Follow   []Follow `json:"follow"`
}

type Twit struct {
	Id        int    `json:"id"`
	Twit      string `json:"name"`
	Userid    int    `json:"userid"`
	CreatedAt string `json:"created_at"`
	User      User   `json:"user"`
}

type Follow struct {
	Id             int    `json:"id"`
	Following      int    `json:"following"`
	Follower       int    `json:"follower"`
	FollowingUsers []User `json:"followingusers"`
	// FollowersUsers []User `json:"followersusers"`
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := ""
	dbName := "godb"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func insertUser(user User) (int64, error) {
	db := dbConn()
	stmt, err := db.Prepare("INSERT INTO user(name, email, password) VALUES(?,?,?)")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(user.Name, user.Email, GetMD5Hash(user.Password))
	if err != nil {
		return -1, err
	}

	return res.LastInsertId()
}

func createTwit(twit Twit) (int64, error) {
	db := dbConn()
	stmt, err := db.Prepare("INSERT INTO twit(twit, userid) VALUES(?, ?)")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(twit.Twit, twit.Userid)
	if err != nil {
		return -1, err
	}

	return res.LastInsertId()
}

func addFollow(follow Follow) (int64, error) {
	user, err := getUserById(follow.Following)
	fmt.Print(user)
	if err != nil {
		if err.Error() == "user not found" {
			return -1, errors.New("following user not exist in user table")
		}
		return -1, err
	}

	db := dbConn()
	// follows := Follow{}
	var id, following, follower int
	errr := db.QueryRow("SELECT * FROM follow WHERE following=? and follower= ?", follow.Following, follow.Follower).Scan(&id, &following, &follower)
	if errr != nil {
		if errr == sql.ErrNoRows {
			stmt, err := db.Prepare("INSERT INTO follow(follower, following) VALUES(?, ?)")
			if err != nil {
				return -1, err
			}
			defer stmt.Close()

			res, err := stmt.Exec(follow.Follower, follow.Following)
			if err != nil {
				return -1, err
			}

			return res.LastInsertId()
		}
		return -1, errr
	}

	return -1, errors.New("Already followed")

}

func loginByToken(token string) (User, error) {
	loginDetails, _ := b64.StdEncoding.DecodeString(token)
	detail := strings.Split(string(loginDetails), ":")
	loginDetail := Login{}
	loginDetail.Email = detail[0]
	loginDetail.Password = detail[1]
	user, err := login(loginDetail)
	return user, err
}

func login(loginDetail Login) (User, error) {
	db := dbConn()
	user := User{}
	var id int
	var name, email, password string
	err := db.QueryRow("SELECT * FROM user WHERE email=?", loginDetail.Email).Scan(&id, &name, &email, &password)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, errors.New("user not found")
		}
		return user, err
	}

	if password == GetMD5Hash(loginDetail.Password) {
		user.Name = name
		user.Email = email
		user.Id = id
		return user, nil
	} else {
		return user, errors.New("Your token is invalid")
	}
}

func getUserByEmail(userEmail string) (int, error) {
	var check int = 0
	db := dbConn()
	stmt, err := db.Query("SELECT * FROM user WHERE email=?", userEmail)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()
	for stmt.Next() {
		check = 1
	}
	return check, nil
}

func getUserById(userId int) (User, error) {
	db := dbConn()
	user := User{}
	var id int
	var name, email, password string
	err := db.QueryRow("SELECT * FROM user WHERE id=?", userId).Scan(&id, &name, &email, &password)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, errors.New("user not found")
		}
		return user, err
	}

	user.Id = id
	user.Name = name
	user.Email = email
	user.Password = password

	return user, nil
}

func getTwitById(twitId int) (Twit, error) {
	db := dbConn()
	twitt := Twit{}
	var id, userid int
	var twit, created_at string
	err := db.QueryRow("SELECT * FROM twit WHERE id=? ORDER BY created_at DESC", twitId).Scan(&id, &twit, &userid, &created_at)
	if err != nil {
		if err == sql.ErrNoRows {
			return twitt, errors.New("user not found")
		}
		return twitt, err
	}

	twitt.Id = id
	twitt.Twit = twit
	twitt.Userid = userid
	twitt.CreatedAt = created_at
	return twitt, nil
}

func getTwitByUserId(userId int) ([]Twit, error) {
	db := dbConn()
	twitt := Twit{}
	twits := []Twit{}
	stmt, err := db.Query("SELECT * FROM twit WHERE userid=? ORDER BY created_at DESC LIMIT 10", userId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		}
		return nil, err
	}

	for stmt.Next() {

		var id, userid int
		var twit, created_at string
		stmt.Scan(&id, &twit, &userid, &created_at)
		twitt.Id = id
		twitt.Twit = twit
		twitt.Userid = userid
		twitt.CreatedAt = created_at
		twits = append(twits, twitt)
	}

	return twits, nil
}

func getFollowingUserList(userId int) ([]Follow, error) {
	db := dbConn()
	follow := Follow{}
	follows := []Follow{}
	stmt, err := db.Query("SELECT * FROM follow where follower=?", userId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		}

		return nil, err
	}

	for stmt.Next() {

		var id, follower, following int
		stmt.Scan(&id, &following, &follower)
		follow.Id = id
		follow.Follower = follower
		follow.Following = following

		follows = append(follows, follow)
	}

	return follows, nil
}

func getUserList(userId int) ([]User, error) {
	db := dbConn()
	user := User{}
	res := []User{}
	stmt, err := db.Query("SELECT * FROM user where id != ?", userId)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	for stmt.Next() {
		var id int
		var name, email, password string
		err = stmt.Scan(&id, &name, &email, &password)
		if err != nil {
			return nil, err
		}
		user.Id = id
		user.Name = name
		user.Email = email
		user.Password = password
		res = append(res, user)
	}
	return res, nil
}

var userType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"email": &graphql.Field{
				Type: graphql.String,
			},
			"password": &graphql.Field{
				Type: graphql.String,
			},
			"twits": &graphql.Field{
				Type: graphql.NewList(twitType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Source.(User).Id
					twits, err := getTwitByUserId(id)
					if err != nil {
						return nil, err
					}
					return twits, nil
				},
			},
		},
	},
)

var twitType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Twit",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"twit": &graphql.Field{
				Type: graphql.String,
			},
			"userid": &graphql.Field{
				Type: graphql.ID,
			},
		},
	},
)

var followType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Follow",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"following": &graphql.Field{
				Type: graphql.Int,
			},
			"follower": &graphql.Field{
				Type: graphql.Int,
			},
			"followinguser": &graphql.Field{
				Type: userType,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Source.(Follow).Following
					followUser, err := getUserById(id)
					if err != nil {
						return nil, err
					}
					return followUser, nil
				},
			},
		},
	},
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			/* Get (read) single user by id
			   http://localhost:8080/api
			   query={user(token:xxxx, id:1){name,email}}
			*/
			"user": &graphql.Field{
				Type:        userType,
				Description: "Get user by id",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
					"token": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},

				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					userId := p.Args["id"].(int)
					userData, err := getUserById(userId)

					if err != nil {
						return nil, err
					} else {
					}
					return userData, nil
				},
			},
			/* Get (read) user list
			   http://localhost:8080/api
			   query={users(token:xxxx){id,name,email}}
			*/
			"users": &graphql.Field{
				Type:        graphql.NewList(userType),
				Description: "Get user list",
				Args: graphql.FieldConfigArgument{
					"token": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					token := params.Args["token"].(string)
					login, err := loginByToken(token)
					if err != nil {
						return nil, err
					}
					fmt.Println(login, "tokenLogin")
					return getUserList(login.Id)
				},
			},
			/* Get (read) user twit list
			   http://localhost:8080/api
			   query={twits(token:xxxx){id}}
			*/
			"twits": &graphql.Field{
				Type:        graphql.NewList(twitType),
				Description: "Get twits by user id",
				Args: graphql.FieldConfigArgument{
					"token": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},

				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					token := p.Args["token"].(string)
					login, err := loginByToken(token)
					twitData, err := getTwitByUserId(login.Id)

					if err != nil {
						return nil, err
					}
					return twitData, nil
				},
			},
			"posts": &graphql.Field{
				Type:        graphql.NewList(followType),
				Description: "Get twits by following user",
				Args: graphql.FieldConfigArgument{
					"token": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},

				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					token := p.Args["token"].(string)
					login, err := loginByToken(token)
					followData, err := getFollowingUserList(login.Id)

					if err != nil {
						return nil, err
					}

					return followData, nil
				},
			},
		},
	})

var mutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Mutation",
	Fields: graphql.Fields{
		/* Create new user item
		http://localhost:8080/api
		query=mutation{create(name:"Sudesh",email:"futuretechdotcodotin@gmail.com")}
		*/
		"createuser": &graphql.Field{
			Type:        graphql.String,
			Description: "Create new user",
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				user := User{
					Id:       0,
					Name:     params.Args["name"].(string),
					Email:    params.Args["email"].(string),
					Password: params.Args["password"].(string),
				}

				checkEmail, err := getUserByEmail(user.Email)

				if checkEmail == 1 {
					return nil, errors.New("Email Already Register")
				}
				if err != nil {
					print(err.Error())
					return nil, errors.New("Please try again server is not responed")
				}

				id, err := insertUser(user)
				if err != nil {
					print(err)
					return nil, errors.New("Please try again server is not responed")
				}
				var userId int = int(id)
				userData, err := getUserById(userId)
				fmt.Print(err)
				if err != nil {
					return nil, errors.New("Please try again server is not responed")
				} else {
					fmt.Print(userData, "user data")
				}

				return "user created successfully", nil
			},
		},

		"createtwit": &graphql.Field{
			Type:        graphql.String,
			Description: "Create new twit",
			Args: graphql.FieldConfigArgument{
				"twit": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"token": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				token := params.Args["token"].(string)

				login, err := loginByToken(token)
				if err != nil {
					return nil, err
				}
				userId := login.Id

				twit := Twit{
					Twit:   params.Args["twit"].(string),
					Userid: userId,
				}

				id, err := createTwit(twit)

				if err != nil {
					println(err.Error())
					return nil, errors.New("Please try again server is not responed")
				} else {
					fmt.Print(id, "created id")
				}

				return "twit created successfuly", nil
			},
		},

		"follow": &graphql.Field{
			Type:        graphql.String,
			Description: "Create new follow",
			Args: graphql.FieldConfigArgument{
				"following": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"token": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				token := params.Args["token"].(string)

				login, err := loginByToken(token)
				if err != nil {
					return nil, err
				}
				userId := login.Id
				following := params.Args["following"].(int)
				follow := Follow{
					Following: following,
					Follower:  userId,
				}

				if userId == following {
					return nil, errors.New("login user id and following user id both are same")
				}

				id, err := addFollow(follow)

				if err != nil {
					println(err.Error())
					return nil, err
				} else {
					fmt.Print(id, "created id")
				}

				return "Followed successfuly", nil
			},
		},
	},
})

var schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    queryType,
		Mutation: mutationType,
	},
)

func executeQuery(query string, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:        schema,
		RequestString: query,
	})
	if len(result.Errors) > 0 {
		fmt.Printf("errors: %v", result.Errors)
	}
	return result
}

func init() {
	twitType.AddFieldConfig("user", &graphql.Field{
		Type: userType,
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			userId := p.Source.(Twit).Userid
			userData, err := getUserById(userId)

			if err != nil {
				return nil, err
			}

			return userData, nil
		},
	},
	)
}

func main() {
	http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			if err := r.ParseForm(); err != nil {
				fmt.Fprintf(w, "ParseForm() err: %v", err)
				return
			}
			result := executeQuery(r.FormValue("query"), schema)
			json.NewEncoder(w).Encode(result)
		} else {
			http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		}
	})

	fmt.Println("Server is running on port 8080")
	http.ListenAndServe(":8080", nil)
}
